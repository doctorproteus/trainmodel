import java.util.HashMap;


public class Murphy
{
	private int failureMode;  
	private HashMap<TrainModel, Integer> failureList = new HashMap<TrainModel, Integer>();
	
	/**
	 * @return the failureMode
	 */
	public int getFailureMode()
	{
		return failureMode;
	}
	/**
	 * @param failureMode the failureMode to set
	 */
	public void setFailureMode(int failureMode)
	{
		this.failureMode = failureMode;
	}
}
