
public class Engine
{
	private double power;
	private double mass;
	private double acceleration;
	private double powerConsumed;
	
	public Engine()
	{
		
	}
	
	/**
	 * @return the power
	 */
	public double getPower()
	{
		return power;
	}
	/**
	 * @param power the power to set
	 */
	public void setPower(double power)
	{
		this.power = power;
	}
	/**
	 * @return the mass
	 */
	public double getMass()
	{
		return mass;
	}
	/**
	 * @param mass the mass to set
	 */
	public void setMass(double mass)
	{
		this.mass = mass;
	}
	/**
	 * @return the acceleration
	 */
	public double getAcceleration()
	{
		return acceleration;
	}
	/**
	 * @param acceleration the acceleration to set
	 */
	public void setAcceleration(double acceleration)
	{
		this.acceleration = acceleration;
	}
	/**
	 * @return the powerConsumed
	 */
	public double getPowerConsumed()
	{
		return powerConsumed;
	}
	/**
	 * @param powerConsumed the powerConsumed to set
	 */
	public void setPowerConsumed(double powerConsumed)
	{
		this.powerConsumed = powerConsumed;
	}
}
